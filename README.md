# CallObserver server

CallObserver is a CRM for logging calls between customers and employees in your organization. CRM has a [mobile application](https://gitlab.com/SolinnenHub/callobserver/client) that logs all incoming and outgoing calls with known clients.

Features:

- Client management
- Employee management
- Call management
- Access management
- Call recording (WIP)

## Demo

![](demo.mp4)


## How to deploy & run

Note: MongoDB server and Redis server must be installed for this project.

```bash
conda env create -f environment.yml
conda activate callobserver

python manage.py migrate
python manage.py createsuperuser --username=admin

python manage.py runserver 0.0.0.0:8000
```