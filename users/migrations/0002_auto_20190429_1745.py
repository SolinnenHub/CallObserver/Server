# Generated by Django 2.1.7 on 2019-04-29 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='permissions',
            field=models.TextField(default='', max_length=4000),
        ),
    ]
