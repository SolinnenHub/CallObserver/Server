from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from jsonfield import JSONField
import json

class CallMode(models.Model):
	name = models.CharField(default='Входящий', max_length=15)

class Position(models.Model):
	name = models.CharField(max_length=80)

	def __str__(self):
		return self.name

class Role(models.Model):
	name = models.CharField(default="Работник", max_length=30)
	permissions = JSONField(default={})
	editable = models.BooleanField(default=True)

	def __str__(self):
		return self.name

class CustomUserManager(UserManager):
	pass

class CustomUser(AbstractUser):
	name = models.CharField(default='', max_length=100)
	position = models.ForeignKey(Position, default=None, null=True, on_delete=models.DO_NOTHING)
	role = models.ForeignKey(Role, default=None, null=True, on_delete=models.CASCADE)
	objects = CustomUserManager()

	def __str__(self):
		if self.name != '':
			return self.name
		return self.username

	def save(self, *args, **kwargs):
		if self.is_superuser:
			adminRoleName = "Администратор"
			if len(Role.objects.filter(name=adminRoleName)) == 0:
				role = Role()
				role.name = adminRoleName
				role.permissions = {
					"CALLS_READ": True,
					"CALLS_ADD": True,
					"CALLS_EDIT": True,
					"CLIENTS_READ": True,
					"CLIENTS_ADD": True,
					"CLIENTS_EDIT": True,
					"CLIENTS_COMMENTS_READ": True,
					"CLIENTS_COMMENTS_ADD": True,
					"CLIENTS_COMMENTS_EDIT": True,
					"USERS_READ": True,
					"USERS_INVITE": True,
					"USERS_EDIT": True,
					"POSITIONS_READ": True,
					"POSITIONS_EDIT": True,
					"CONFIG_EDIT": True,
				}
				role.editable = False
				role.save()
			if len(CallMode.objects.all()) != 2:
				c1 = CallMode()
				c1.name = "Входящий"
				c1.save()
				c2 = CallMode()
				c2.name = "Исходящий"
				c2.save()
			self.role = Role.objects.filter(name=adminRoleName)[0]
		super(CustomUser, self).save(*args, **kwargs)

class Invite(models.Model):
	key = models.CharField(default=None, null=True, max_length=100)
	name = models.CharField(default='', max_length=100)
	position = models.ForeignKey(Position, default=None, null=True, on_delete=models.DO_NOTHING)
	role = models.ForeignKey(Role, default=None, null=True, on_delete=models.CASCADE)

	def __str__(self):
		return self.name
