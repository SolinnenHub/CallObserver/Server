from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from .models import CustomUser

class LoginForm(AuthenticationForm):
	username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'placeholder': 'Username', 'style':'display:none;'}), max_length=64)
	password = forms.CharField(label='Пароль', strip=False, widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'style':'display:none;'}), max_length=250)

	class Meta:
		model = CustomUser
		fields = ('username', 'password')

class JoinForm(UserCreationForm):
	key = forms.CharField(label='Ключ доступа', widget=forms.TextInput(attrs={'class': 'form-control'}))
	username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-control'}), min_length=3, max_length=64)
	password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-control'}), min_length=8, max_length=250)
	password2 = forms.CharField(label='Подтверждение пароля', widget=forms.PasswordInput(attrs={'class': 'form-control'}), min_length=8, max_length=250)

	class Meta(UserCreationForm):
		model = CustomUser
		fields = ('key', 'username', 'password1', 'password2')

	def clean(self):
		username = self.cleaned_data.get(CustomUser.USERNAME_FIELD)
		if username and CustomUser.objects.filter(username__iexact=username).exists():
			self.add_error('username', 'Такой пользователь уже существует.')
		super(JoinForm, self).clean()

class CustomUserChangeForm(UserChangeForm):
	class Meta:
		model = CustomUser
		fields = UserChangeForm.Meta.fields

class PasswordChangeForm(forms.Form):
	oldpassword = forms.CharField(label='Текущий пароль', max_length=20, widget=forms.PasswordInput(attrs={'placeholder':'Old Password'}))
	newpassword1 = forms.CharField(label='Новый пароль', max_length=20, widget=forms.PasswordInput(attrs={'placeholder':'New Password'}))
	newpassword2 = forms.CharField(label='Повтор пароля', max_length=20, widget=forms.PasswordInput(attrs={'placeholder':'Confirm New Password'}))

	def clean(self):
		if 'newpassword1' in self.cleaned_data and 'newpassword2' in self.cleaned_data:
			if self.cleaned_data['newpassword1'] != self.cleaned_data['newpassword2']:
				raise forms.ValidationError("Пароли не совпадают.")
		return self.cleaned_data
