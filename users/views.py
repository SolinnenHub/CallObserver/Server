from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.contrib.auth import authenticate, login as customlogin
from .forms import *
from .models import CustomUser, Position, Role, Invite
from mainApp.views import noAccessToViewPage, noAccessToExecute, getFormErrors

def authView(request):
	if request.method == 'GET':
		return render(request, 'users/login.html', {'form': LoginForm()})

	form = LoginForm(data=request.POST)
	if form.is_valid():
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(request, username=username, password=password)
		if user is not None:
			customlogin(request, user)
			return HttpResponse('Вы успешно вошли в систему.', status=200)
		return HttpResponse('Вы указали неверное имя пользователя или пароль. Учтите, что пароль чувствителен к регистру.', status=403)
	return HttpResponse(form.non_field_errors(), status=400)

def getSession(request):
	if request.method == 'GET':
		if request.GET.get('sessionId', None):
			return redirect('/')
		if request.user.is_authenticated:
			sessionId = request.session._SessionBase__session_key
			return render(request, 'users/getSession.html', {'sessionId': sessionId})
	return HttpResponseForbidden("Вы не авторизованы.")

def joinView(request):
	if request.method == 'GET':
		return render(request, 'users/join.html', {'form': JoinForm()})

	form = JoinForm(data=request.POST)
	if form.is_valid():
		inv = None
		try:
			try:
				inv = Invite.objects.get(key=form.cleaned_data['key'])
			except Exception:
				return HttpResponse('Ошибка: ключ недействителен.', status=403)
			form.save()

			user = CustomUser.objects.get(username=form.cleaned_data['username'])
			user.name = inv.name
			user.position = inv.position
			user.role = inv.role
			user.save()
			inv.delete()
			return HttpResponse('Регистрация успешно завершена. Теперь вы можете войти в систему.', status=200)
		except Exception as e:
			return HttpResponse('Ошибка: '+str(e), status=403)
	return getFormErrors(form)

def settingsView(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'users/settings.html', {
				'passwordChange_form': PasswordChangeForm(),
				'general_form': GeneralForm(),
				'positions': Position.objects.all(),
			})

		if request.POST['act'] == 'general':
			form = GeneralForm(data=request.POST)
			if form.is_valid():
				user = request.user
				user.name = form.cleaned_data['name']
				positions = Position.objects.filter(name=form.cleaned_data['position'])
				if len(positions) != 1:
					return HttpResponse('Ошибка: должность не существует или дублируется.', status=400)
				user.position = positions[0]
				user.save()
				return HttpResponse('Изменения сохранены', status=200)
			return HttpResponse(form.non_field_errors(), status=400)

		if request.POST['act'] == 'changePassword':
			form = PasswordChangeForm(data=request.POST)
			if form.is_valid():
				username = request.user.username
				password = form.cleaned_data['oldpassword']
				newpassword = form.cleaned_data['newpassword1']
				user = authenticate(username=username, password=password)
				if user is not None:
					user.set_password(newpassword)
					user.save()
					return HttpResponse('Ваш пароль успешно изменен.', status=200)
				return HttpResponse('Текущий пароль указан неверно.', status=403)
			return HttpResponse(form.non_field_errors(), status=400)
	return HttpResponseForbidden("Вы не авторизованы.")
