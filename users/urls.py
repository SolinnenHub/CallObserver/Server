from django.urls import include, path
from . import views

app_name = 'users'
urlpatterns = [
	path('auth/', views.authView, name='auth'),
	path('getSession/', views.getSession),
	path('join/', views.joinView, name='join'),
	path('settings/', views.settingsView, name='settings'),
	path('', include('django.contrib.auth.urls')),
]