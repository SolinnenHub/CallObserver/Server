from django.urls import path
from . import views

app_name = 'mainApp'
urlpatterns = [
	path('', views.calls, name='index'),

	path('calls/', views.calls, name='calls'),
	path('calls/call/', views.callAdd, name='callAdd'),
	path('calls/call/<int:callId>/edit/', views.callEdit, name='callEdit'),
	path('calls/call/<int:callId>/delete/', views.callDelete, name='callDelete'),
	path('calls/call/<int:callId>/uploadRecord/', views.callRecordUpload, name='callRecordUpload'),

	path('clients/', views.clients, name='clients'),
	path('clients/client/', views.clientAdd, name='clientAdd'),
	path('clients/client/<int:clientId>/edit/', views.clientEdit, name='clientEdit'),
	path('clients/client/<int:clientId>/delete/', views.clientDelete, name='clientDelete'),
	path('clients/getByNumber/<str:number>/', views.clientGetByNumber, name='clientGetByNumber'),

	path('clients/client/<int:clientId>/comment/', views.commentAdd, name='commentAdd'),
	path('clients/client/<int:clientId>/comment/<int:commentId>/edit/', views.commentEdit, name='commentEdit'),
	path('clients/client/<int:clientId>/comment/<int:commentId>/delete/', views.commentDelete, name='commentDelete'),

	path('users/', views.users, name='users'),
	path('users/user/<int:userId>/edit/', views.userEdit, name='userEdit'),
	path('users/user/<int:userId>/delete/', views.userDelete, name='userDelete'),

	path('invites/inv/', views.invAdd, name='invAdd'),
	path('invites/inv/<int:invId>/edit/', views.invEdit, name='invEdit'),
	path('invites/inv/<int:invId>/delete/', views.invDelete, name='invDelete'),

	path('positions/', views.positions, name='positions'),
	path('positions/position/', views.positionAdd, name='positionAdd'),
	path('positions/position/<int:positionId>/edit/', views.positionEdit, name='positionEdit'),
	path('positions/position/<int:positionId>/delete/', views.positionDelete, name='positionDelete'),

	path('config/', views.config, name='config'),
	path('config/role/', views.roleAdd, name='roleAdd'),
	path('config/role/<int:roleId>/edit/', views.roleEdit, name='roleEdit'),
	path('config/role/<int:roleId>/delete/', views.roleDelete, name='roleDelete'),

	path('helpSrc/', views.helpSrc, name='helpSrc'),
]
