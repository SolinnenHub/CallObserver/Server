from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from users.models import Role, Position, CustomUser, Invite, CallMode
from django.conf import settings
from .models import Client, Call, Comment
from .forms import RoleForm, PositionForm, UserForm, ClientForm, CommentForm, CallForm
import uuid, json, time
from operator import attrgetter
import os, base64, shutil
from pydub import AudioSegment

noAccessToViewPage = "У вас недостаточно прав для просмотра этой страницы."
noAccessToExecute = "У вас недостаточно прав для выполнения этого действия."

def isFileExists(path):
	if os.path.isfile(path):
		return True
	elif os.path.isdir(path):
		return True
	return False

def deleteFile(path):
	if os.path.isfile(path):
		os.remove(path)
	elif os.path.isdir(path):
		shutil.rmtree(path)
	else:
		raise FileNotFoundError("No such file or directory: '"+path+"'")


def getFormErrors(form):
	d = form.errors.get_json_data()
	res = ''
	for k in d:
		for item in d[k]:
			res += '<p>'+str(item['message'])+'</p>'
	return HttpResponse(res, status=403)

def calls(request):
	if request.user.is_authenticated:
		if request.user.role.permissions.get('CALLS_READ', None):
			return render(request, 'mainApp/calls.html', {
				'calls': Call.objects.all().order_by('time').reverse(),
			})
	return render(request, 'mainApp/error.html', {'err': noAccessToViewPage})

def callAdd(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/callAdd.html', {
				'clients': Client.objects.all().order_by('lastUpdateTime').reverse(),
				'modes': CallMode.objects.all()
			})

		if request.user.role.permissions.get('CALLS_ADD', None):
			form = CallForm(data=request.POST)
			if form.is_valid():
				call = Call()
				try:
					client = Client.objects.get(id=form.cleaned_data['clientId'])
					client.lastUpdateTime = time.time()
					client.save()
					call.client = client
				except Exception:
					return HttpResponse('Ошибка: клиента не существует.', status=403)
				try:
					call.mode = CallMode.objects.get(id=form.cleaned_data['modeId'])
				except Exception:
					return HttpResponse('Ошибка: данного типа звонка не существует.', status=403)
				call.user = request.user
				call.time = time.time()
				call.save()
				return HttpResponse('Успешное добавление. №'+str(call.id), status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def callEdit(request, callId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			try:
				filename = settings.MEDIA_URL+str(callId)+".mp3"
				return render(request, 'mainApp/callEdit.html', {
					'call': Call.objects.get(id=callId),
					'clients': Client.objects.all().order_by('lastUpdateTime').reverse(),
					'record_exists': isFileExists(filename),
					'modes': CallMode.objects.all()
				})
			except Exception:
				return render(request, 'mainApp/clientEdit.html', {'err': "Ошибка: звонка не существует."})
		
		if request.user.role.permissions.get('CALLS_EDIT', None):
			form = CallForm(data=request.POST)
			if form.is_valid():
				try:
					call = Call.objects.get(id=callId)
					try:
						call.client = Client.objects.get(id=form.cleaned_data['clientId'])
					except Exception:
						return HttpResponse('Ошибка: клиента не существует.', status=403)
					try:
						call.mode = CallMode.objects.get(id=form.cleaned_data['modeId'])
					except Exception:
						return HttpResponse('Ошибка: данного типа звонка не существует.', status=403)
					call.save()
					return HttpResponse('Изменения сохранены.', status=200)
				except Exception:
					return HttpResponse('Ошибка: звонка не существует.', status=403)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def callDelete(request, callId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('CALLS_EDIT', None):
				try:
					Call.objects.get(id=callId).delete()
					return HttpResponse('Успешное удаление.', status=200)
				except Exception:
					return HttpResponse('Ошибка: звонка не существует.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def callRecordUpload(request, callId):
	b64encodedRecord = request.POST.get("b64encodedRecord", "")
	if request.method == 'POST' and b64encodedRecord != "":

		recordBytes = None
		try:
			recordBytes = base64.b64decode(b64encodedRecord)
		except Exception:
			return HttpResponse('Ошибка: сервер не может декодировать полученную запись.', status=403)

		filename = settings.MEDIA_URL+str(callId)
		with open(filename+".mp4", 'wb') as output:
			output.write(recordBytes)

		wav_audio = AudioSegment.from_file(filename+".mp4", format="mp4")
		wav_audio.export(filename+".mp3", format="mp3")
		deleteFile(filename+".mp4")

		try:
			call = Call.objects.get(id=callId)
			try:
				call.record = filename+".mp3"
			except Exception as e:
				print(e)
				return HttpResponse('Ошибка: запись звонка не найдена на сервере.', status=403)
			call.save()
			return HttpResponse('Успех.', status=200)
		except Exception:
			return HttpResponse('Ошибка: звонка не существует.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def clients(request):
	if request.user.is_authenticated:
		if request.user.role.permissions.get('CLIENTS_READ', None):
			return render(request, 'mainApp/clients.html', {
				'clients': Client.objects.all().order_by('lastUpdateTime').reverse()
			})
	return render(request, 'mainApp/error.html', {'err': noAccessToViewPage})

def clientAdd(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/clientAdd.html')

		if request.user.role.permissions.get('CLIENTS_ADD', None):
			form = ClientForm(data=request.POST)
			if form.is_valid():
				client = Client()
				client.name = form.cleaned_data['name']
				client.number = form.cleaned_data['number']
				if len(Client.objects.filter(number=client.number)) != 0:
					return HttpResponse('Ошибка: клиент с таким номером уже существует.', status=403)
				client.lastUpdateTime = time.time()
				client.save()
				return HttpResponse('Успешное добавление.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def clientEdit(request, clientId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			try:
				client = Client.objects.get(id=clientId)
				return render(request, 'mainApp/clientEdit.html', {
					'client': client,
					'comments': Comment.objects.filter(client=client).order_by('time').reverse()
				})
			except Exception:
				return render(request, 'mainApp/clientEdit.html', {'err': "Ошибка: клиента не существует."})
		
		if request.user.role.permissions.get('CLIENTS_EDIT', None):
			client = None
			try:
				client = Client.objects.get(id=clientId)
			except Exception:
				return HttpResponse('Ошибка: клиента не существует.', status=403)

			form = ClientForm(data=request.POST)
			if form.is_valid():
				try:
					client.name = form.cleaned_data['name']
					if len(Client.objects.filter(number=form.cleaned_data['number'])) != 0 and form.cleaned_data['number'] != client.number:
						return HttpResponse('Ошибка: клиент с таким номером уже существует.', status=403)
					client.number = form.cleaned_data['number']
					client.lastUpdateTime = time.time()
					client.save()
					return HttpResponse('Изменения сохранены.', status=200)
				except Exception as e:
					return HttpResponse('Ошибка: '+str(e), status=403)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def clientDelete(request, clientId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('CLIENTS_EDIT', None):
				try:
					Client.objects.get(id=clientId).delete()
					return HttpResponse('Успешное удаление.', status=200)
				except Exception:
					return HttpResponse('Ошибка: клиента не существует.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def clientGetByNumber(request, number):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('CLIENTS_READ', None):
				d = {
					"id": -1,
					"name": "",
					"err": ""
				}
				try:
					client = Client.objects.get(number=number)
					d["id"] = client.id
					d["name"] = client.name
					return JsonResponse(d);
				except Exception:
					d["err"] = "NO_SUCH_CLIENT"
					return JsonResponse(d);
	return HttpResponseForbidden(noAccessToExecute)

def commentAdd(request, clientId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('CLIENTS_COMMENTS_ADD', None):
				form = CommentForm(data=request.POST)
				if form.is_valid():
					comment = Comment()
					try:
						client = Client.objects.get(id=clientId)
						client.lastUpdateTime = time.time()
						client.save()
						comment.client = client
					except Exception:
						return HttpResponse('Ошибка: клиента не существует.', status=400)
					comment.label = form.cleaned_data['label']
					comment.text = form.cleaned_data['text']
					comment.user = request.user
					comment.time = time.time()
					comment.save()
					return HttpResponse('Комментарий добавлен.', status=200)
				return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def commentEdit(request, clientId, commentId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			form = CommentForm(data=request.POST)
			if form.is_valid():
				comment = None
				try:
					comment = Comment.objects.get(id=commentId)
				except Exception:
					return HttpResponse('Ошибка: комментарий не существует.', status=403)

				if request.user.role.permissions.get('CLIENTS_COMMENTS_EDIT', None) or (request.user.role.permissions.get('CLIENTS_COMMENTS_ADD', None) and request.user == comment.user):
					comment = Comment.objects.get(id=commentId)
					comment.label = form.cleaned_data['label']
					comment.text = form.cleaned_data['text']
					comment.time = time.time()
					comment.client.lastUpdateTime = time.time()
					comment.client.save()
					comment.save()
					return HttpResponse('Комментарий сохранен.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def commentDelete(request, clientId, commentId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			comment = None
			try:
				comment = Comment.objects.get(id=commentId)
			except Exception:
				return HttpResponse('Ошибка: комментарий не существует.', status=403)

			if request.user.role.permissions.get('CLIENTS_COMMENTS_EDIT', None):
				comment.delete()
				return HttpResponse('Комментарий удален.', status=200)
			if request.user.role.permissions.get('CLIENTS_COMMENTS_ADD', None) and request.user == comment.user:
				comment.delete()
				return HttpResponse('Комментарий удален.', status=200)
	return HttpResponseForbidden(noAccessToExecute)

def users(request):
	if request.user.is_authenticated:
		if request.user.role.permissions.get('USERS_READ', None):
			return render(request, 'mainApp/users.html', {
				'users': CustomUser.objects.all(),
				'invites': Invite.objects.all(),
			})
	return render(request, 'mainApp/error.html', {'err': noAccessToViewPage})

def userEdit(request, userId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			try:
				return render(request, 'mainApp/userEdit.html', {
					'exUser':  CustomUser.objects.get(id=userId),
					'positions': Position.objects.all(),
					'roles': Role.objects.all(),
				})
			except Exception:
				return render(request, 'mainApp/userEdit.html', {'err': "Ошибка: пользователь не существует."})			

		if request.user.role.permissions.get('USERS_EDIT', None):
			form = UserForm(data=request.POST)
			if form.is_valid():
				user = None
				try:
					user = CustomUser.objects.get(id=userId)
				except Exception:
					return HttpResponse('Ошибка: пользователь не существует.', status=403)
				user.name = form.cleaned_data['name']
				try:
					user.position = Position.objects.get(name=form.cleaned_data['position'])
				except Exception:
					return HttpResponse('Ошибка: должность не существует или дублируется.', status=400)
				try:
					user.role = Role.objects.get(name=form.cleaned_data['role'])
				except Exception:
					return HttpResponse('Ошибка: группа не существует или дублируется.', status=400)
				user.save()
				return HttpResponse('Изменения сохранены.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def userDelete(request, userId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('POSITIONS_EDIT', None):
				CustomUser.objects.get(id=userId).delete()
				return HttpResponse('Успешное удаление.', status=200)
	return HttpResponseForbidden(noAccessToExecute)

def invAdd(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/invAdd.html', {
				'positions': Position.objects.all(),
				'roles': Role.objects.all(),
			})
		
		if request.user.role.permissions.get('USERS_INVITE', None):
			form = UserForm(data=request.POST)
			if form.is_valid():
				inv = Invite()
				inv.key = uuid.uuid4().hex
				inv.name = form.cleaned_data['name']
				try:
					inv.position = Position.objects.get(name=form.cleaned_data['position'])
				except Exception:
					return HttpResponse('Ошибка: должность не существует или дублируется.', status=400)
				try:
					inv.role = Role.objects.get(name=form.cleaned_data['role'])
				except Exception:
					return HttpResponse('Ошибка: группа не существует или дублируется.', status=400)
				inv.save()
				return HttpResponse(inv.key, status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def invEdit(request, invId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			try:
				return render(request, 'mainApp/invEdit.html', {
					'inv': Invite.objects.get(id=invId),
					'positions': Position.objects.all(),
					'roles': Role.objects.all(),
				})
			except Exception:
				return render(request, 'mainApp/invEdit.html', {'err': "Ошибка: приглашение не существует."})

		if request.user.role.permissions.get('USERS_INVITE', None):
			form = UserForm(data=request.POST)
			if form.is_valid():
				inv = None
				try:
					inv = Invite.objects.get(id=invId)
				except Exception:
					return HttpResponse('Ошибка: приглашение не существует.', status=403)
				inv.name = form.cleaned_data['name']
				try:
					inv.position = Position.objects.get(name=form.cleaned_data['position'])
				except Exception:
					return HttpResponse('Ошибка: должность не существует или дублируется.', status=400)
				try:
					inv.role = Role.objects.get(name=form.cleaned_data['role'])
				except Exception:
					return HttpResponse('Ошибка: группа не существует или дублируется.', status=400)
				inv.save()
				return HttpResponse('Изменения сохранены.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def invDelete(request, invId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('USERS_INVITE', None):
				try:
					Invite.objects.get(id=invId).delete()
					return HttpResponse('Успешное удаление.', status=200)
				except Exception:
					return HttpResponse('Ошибка: приглашение не существует.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def positions(request):
	if request.user.is_authenticated:
		if request.user.role.permissions.get('POSITIONS_READ', None):
			return render(request, 'mainApp/positions.html', {'positions': Position.objects.all()})
	return render(request, 'mainApp/error.html', {'err': noAccessToViewPage})

def positionAdd(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/positionAdd.html')

		if request.user.role.permissions.get('POSITIONS_EDIT', None):
			form = PositionForm(data=request.POST)
			if form.is_valid():
				if len(Position.objects.filter(name=form.cleaned_data['name'])) != 0:
					return HttpResponse('Ошибка: должность с таким названием уже существует.', status=403)
				position = Position()
				position.name = form.cleaned_data['name']
				position.save()
				return HttpResponse('Успешное добавление.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def positionEdit(request, positionId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			try:
				position = Position.objects.get(id=positionId)
				return render(request, 'mainApp/positionEdit.html', {'position': position})
			except Exception:
				return render(request, 'mainApp/positionEdit.html', {'err': "Ошибка: такой должности не существует."})

		if request.user.role.permissions.get('POSITIONS_EDIT', None):
			form = PositionForm(data=request.POST)
			if form.is_valid():
				try:
					position = Position.objects.get(id=positionId)
					position.name = form.cleaned_data['name']
					position.save()
					return HttpResponse('Изменения сохранены.', status=200)
				except Exception as e:
					return HttpResponse('Ошибка: '+str(e), status=403)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def positionDelete(request, positionId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('POSITIONS_EDIT', None):
				try:
					Position.objects.get(id=positionId).delete()
					return HttpResponse('Успешное удаление.', status=200)
				except Exception:
					return HttpResponse('Ошибка: должность не существует.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def config(request):
	if request.user.is_authenticated:
		if request.user.role.permissions.get('CONFIG_EDIT', None):
			return render(request, 'mainApp/config.html', {'roles': Role.objects.all()})
	return render(request, 'mainApp/error.html', {'err': "У вас недостаточно прав для просмотра этой страницы."})

def roleAdd(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/roleAdd.html')

		if request.user.role.permissions.get('CONFIG_EDIT', None):
			form = RoleForm(data=request.POST)
			if form.is_valid():
				if len(Role.objects.filter(name=form.cleaned_data['name'])) != 0:
					return HttpResponse('Ошибка: группа с таким названием уже существует.', status=403)

				role = Role()
				role.name = form.cleaned_data['name']
				try:
					role.permissions = json.loads(form.cleaned_data['permissions'])
				except Exception:
					return HttpResponse('Невозможно прочесть разрешения. Проверьте правильность написания.', status=403)
				role.save()
				return HttpResponse('Успешное добавление.', status=200)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def roleEdit(request, roleId):
	if request.user.is_authenticated:
		if request.method == 'GET':
			role = None
			try:
				role = Role.objects.get(id=roleId)
			except Exception:
				return render(request, 'mainApp/roleEdit.html', {'err': "Ошибка: такой группы не существует."})

			rolePermissionsJson = json.dumps(role.permissions)
			return render(request, 'mainApp/roleEdit.html', {'role': role, 'rolePermissionsJson': rolePermissionsJson})
		
		if request.user.role.permissions.get('CONFIG_EDIT', None):
			role = None
			try:
				role = Role.objects.get(id=roleId)
			except Exception:
				return HttpResponse('Ошибка: такой группы не существует.', status=403)

			if not role.editable:
				return HttpResponse('Эту группу нельзя редактировать.', status=403)

			form = RoleForm(data=request.POST)
			if form.is_valid():
				try:
					role.name = form.cleaned_data['name']
					role.permissions = json.loads(form.cleaned_data['permissions'])
					role.save()
					return HttpResponse('Изменения сохранены.', status=200)
				except Exception as e:
					return HttpResponse('Ошибка: '+str(e), status=403)
			return getFormErrors(form)
	return HttpResponseForbidden(noAccessToExecute)

def roleDelete(request, roleId):
	if request.user.is_authenticated:
		if request.method == 'POST':
			if request.user.role.permissions.get('CONFIG_EDIT', None):
				role = None
				try:
					role = Role.objects.get(id=roleId)
				except Exception:
					return HttpResponse('Ошибка: группа не существует.', status=403)
				if role.editable:
					role.delete()
					return HttpResponse('Успешное удаление.', status=200)
				return HttpResponse('Эту группу нельзя удалять.', status=403)
	return HttpResponseForbidden(noAccessToExecute)

def helpSrc(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			return render(request, 'mainApp/help_src.html')
	return HttpResponseForbidden(noAccessToExecute)
