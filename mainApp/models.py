from django.db import models
from jsonfield import JSONField
from users.models import CustomUser, CallMode

class Client(models.Model):
	name = models.CharField(default='', max_length=150)
	number = models.CharField(max_length=60)
	lastUpdateTime = models.IntegerField(default=-1)

	def __str__(self):
		if self.name != '':
			return self.name
		return self.number

class Call(models.Model):
	client = models.ForeignKey(Client, default=None, null=True, on_delete=models.CASCADE)
	user = models.ForeignKey(CustomUser, default=None, null=True, on_delete=models.CASCADE)
	time = models.IntegerField(default=-1)
	duration = models.CharField(default='00:00', max_length=7)
	mode = models.ForeignKey(CallMode, default=None, null=True, on_delete=models.CASCADE)
	record = models.FileField(default=None, null=True, upload_to='records/')

class Comment(models.Model):
	client = models.ForeignKey(Client, default=None, null=True, on_delete=models.CASCADE)
	user = models.ForeignKey(CustomUser, default=None, null=True, on_delete=models.CASCADE)
	time = models.IntegerField(default=-1)
	label = models.TextField(default='', max_length=40)
	text = models.TextField(default='', max_length=20000)

	def __str__(self):
		return self.text
