from django.contrib import admin
from .models import *

admin.site.register(Client)
admin.site.register(Call)
admin.site.register(Comment)