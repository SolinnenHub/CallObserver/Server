from django import forms
from users.models import Role, Position
from jsonfield import JSONField

class RoleForm(forms.Form):
	name = forms.CharField(required=True, max_length=30, widget=forms.TextInput())
	permissions = forms.CharField(required=True, widget=forms.TextInput())

class PositionForm(forms.Form):
	name = forms.CharField(required=True, max_length=80, widget=forms.TextInput())

class UserForm(forms.Form):
	name = forms.CharField(required=True, max_length=100, widget=forms.TextInput())
	position = forms.CharField(required=True, widget=forms.Select())
	role = forms.CharField(required=True, widget=forms.Select())

class ClientForm(forms.Form):
	name = forms.CharField(required=False, max_length=150, widget=forms.TextInput())
	number = forms.CharField(required=True, max_length=60, widget=forms.TextInput())

class CommentForm(forms.Form):
	label = forms.CharField(required=True, max_length=40, widget=forms.TextInput())
	text = forms.CharField(required=True, max_length=20000, widget=forms.TextInput())

class CallForm(forms.Form):
	clientId = forms.CharField(required=True, max_length=10, widget=forms.TextInput())
	modeId = forms.CharField(required=True, max_length=10, widget=forms.TextInput())